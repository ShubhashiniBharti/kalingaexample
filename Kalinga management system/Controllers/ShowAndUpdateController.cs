﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kalinga_management_system.Models;

namespace Kalinga_management_system.Controllers
{
    public class ShowAndUpdateController : Controller
    {
        // GET: ShowAndUpdate
        private readonly ManagerModel managerModel = new ManagerModel();


        [HttpGet]
        public ActionResult GetLeadModelDetail()
        {
            try
            {
                List<LeadModel> leadModels = managerModel.GetAllLeadsDetailsFromDatabaseDatabase();
                return View(leadModels);

            }
            catch (Exception e)
            {
                ViewBag.message = e.Message;
                return View();
            }
        }


        [HttpGet]
        public ActionResult UpdateLeadDetail(int id)
        {
            return View(managerModel.GetAllLeadsDetailsFromDatabase().Find(LeadModel => LeadModel.LeadId == id));
        }

        [HttpPost]
        public ActionResult UpdateLeadDetail(LeadModel leadModel)
        {

            int rowAffected = managerModel.UpdateLeadDetail(leadModel);

            if (rowAffected > 0)
            {
                return View();
            }
            else
            {
                ViewBag.ErrorMessage = "Failed to Update Details Try again ! ! !";
                return View(managerModel.GetAllLeadsDetailsFromDatabase().Find(LeadModel => leadModel.LeadId == leadModel.LeadId));
            }
        }




        
        [HttpGet]

        public ActionResult MindModelDetail()
        {
            try
            {
                List<MindModel> mindModels = managerModel.GetAllMindsDetailsFromDatabase();
                return View(mindModels);
            }
            catch (Exception ex)
            {

                ViewBag.message = ex.Message;
                return View();
            }
        }

        [HttpGet]
        public ActionResult UpdateMindDetail(int id)
        {
            ViewBag.Leads = managerModel.GetAllLeadsDetailsFromDatabase();
            return View(managerModel.GetAllMindsDetailsFromDatabase().Find(MindModel => MindModel.MindId == id));
        }

        [HttpPost]
        public ActionResult UpdateMindDetail(MindModel mindModel, LeadModel leadModel)
        {
            ViewBag.Leads = managerModel.GetAllLeadsDetailsFromDatabase();
            int rowAffected = managerModel.UpdateMindDetail(mindModel, leadModel);

            if (rowAffected > 0)
            {
                return View();
            }
            else
            {
                ViewBag.ErrorMessage = "Failed to Update Details Try again ! ! !";
                ViewBag.Leads = managerModel.GetAllLeadsDetailsFromDatabase();
                return View(managerModel.GetAllMindsDetailsFromDatabase().Find(MindModel => MindModel.MindId == leadModel.LeadId));
            }
        }
    }
}