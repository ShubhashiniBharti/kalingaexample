﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kalinga_management_system.Models;

namespace Kalinga_management_system.Controllers
{
    public class AddUserController : Controller
    {
        // GET: AddUser

        private readonly ManagerModel managerModel = new ManagerModel();

        [HttpGet]
        public ActionResult GetAndAddLead()
        {
            try
            {
                LeadModel leadModel = new LeadModel();

                return View(leadModel);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;

                return View();
            }

            
        }
        [HttpPost]
        public ActionResult GetAndAddLead(LeadModel lead)
        {
            try
            {
                int value = managerModel.AddLeadDetail(lead);

                if (value > 0)
                {

                    return View();
                }
                else
                {
                    ViewBag.message = "Failed to add data into database !!!";
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;

                return View();
            }

            
        }





        [HttpGet]
        public ActionResult GetAndAddMind()
        {
            try
            {
                MindModel mindModel = new MindModel();
                List<LeadModel> context = managerModel.GetAllLeadsDetailsFromDatabase();

                if (context != null)
                {
                    ViewBag.leads = context;
                }
                return View();
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;

                return View();
            }
            
        }


        [HttpPost]
        public ActionResult GetAndAddMind(MindModel mindModel, LeadModel leadModel)
        {
            try
            {
                ViewBag.leads = managerModel.GetAllLeadsDetailsFromDatabase();

                int value = managerModel.AddMindDetail(mindModel, leadModel);
                if (value > 0)
                {
                    return View();
                }
                else
                {
                    ViewBag.message = "Failed to add data into database !!!";
                    return View();
                }
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = e.Message;

                return View();
            }

           
        }


    }
}