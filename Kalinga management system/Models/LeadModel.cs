﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kalinga_management_system.Models
{
    public class LeadModel
    {
        [DisplayName("Id")]
        public int LeadId { get; set; }

        [DisplayName("Enter Lead Name : ")]
        [Required(ErrorMessage = "Name is required")]
        [RegularExpression("^[a-zA-Z_ ]*$", ErrorMessage = "Please enter all character")]
        public string LeadName { get; set; }



        [DisplayName("Enter Lead Contact Number: ")]
        [Required(ErrorMessage = "Contact number is required")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "Please enter Contact of user in valid format")]
        public long LeadContactNumber { get; set; }




        [DisplayName("Select One Category for the Lead: ")]
        public string Category { get; set; }
    }
}