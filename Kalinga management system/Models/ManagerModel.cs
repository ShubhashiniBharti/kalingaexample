﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;
using KalingaManagementSystem.BusinessLayer;

namespace Kalinga_management_system.Models
{
    public class ManagerModel
    {
        private KalingaBL kalingaBl = new KalingaBL();
        private Mapping mapping = new Mapping();
        public int AddLeadDetail(LeadModel lead)
        {
            return kalingaBl.AddLeadDetails(mapping.ConvertLeadModelToEntity(lead));
        }

        public List<LeadModel> GetAllLeadsDetailsFromDatabase()
        {
            List<Lead> leads = kalingaBl.GetAllLeadsDetailsFromDatabase();
            List<LeadModel> leadModels = new List<LeadModel>();

            foreach (var lead in leads)
            {
                leadModels.Add(AssignAllLeadsDetail(lead));
            }

            return leadModels;


        }

        private LeadModel AssignAllLeadsDetail(Lead lead)
        {
            LeadModel leadModel = new LeadModel();
            leadModel.LeadId = lead.LeadId;
            leadModel.LeadName = lead.LeadName;
            leadModel.LeadContactNumber = lead.LeadContactNumber;
            leadModel.Category = lead.Category;

            return leadModel;
        }

        public int AddMindDetail(MindModel mindModel, LeadModel leadModel)
        {
            return kalingaBl.AddMindDetail(mapping.ConvertMindModelToEntity(mindModel), mapping.ConvertLeadModelToEntity(leadModel));
        }

        public List<LeadModel> GetAllLeadsDetailsFromDatabaseDatabase()
        {
            List<Lead> leads = kalingaBl.GetAllLeadsDetailsFromDatabaseDatabase();
            List<LeadModel> leadModels = mapping.ConvertLeadFromEntityToModelList(leads);
            return leadModels;
        }

        public int UpdateLeadDetail(LeadModel leadModel)
        {
            return kalingaBl.UpdateLeadDetail(mapping.ConvertLeadModelToEntity(leadModel));
        }

        public List<MindModel> GetAllMindsDetailsFromDatabase()
        {
            List<Mind> minds = kalingaBl.GetAllMindsDetailsFromDatabase();
            List<MindModel> mindModels = mapping.ConvertMindFromEntityToModelList(minds);
            return mindModels;
        }

        public int UpdateMindDetail(MindModel mindModel, LeadModel leadModel)
        {
            return kalingaBl.UpdateMindDetail(mapping.ConvertLeadModelToEntity(leadModel), mapping.ConvertMindModelToEntity(mindModel));

        }
    }
}