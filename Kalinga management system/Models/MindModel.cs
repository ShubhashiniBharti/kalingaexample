﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Kalinga_management_system.Models
{
    public class MindModel
    {
        [DisplayName("Id")]
        public int MindId { get; set; }

        [DisplayName("Enter Mind Name : ")]
        [Required(ErrorMessage = "Name is required")]
        [RegularExpression("^[a-zA-Z_ ]*$", ErrorMessage = "Please enter all valid characters only")]
        public string MindName { get; set; }



        [DisplayName("Enter Mind Date Of Joining : ")]
        [Required(ErrorMessage = "Date of Joining is required")]
        public DateTime DOJ { get; set; }



        [DisplayName("Enter Mind Contact Number: ")]
        [Required(ErrorMessage = "Contact number is required")]
        [RegularExpression("^[0-9]{10}$", ErrorMessage = "Please enter in contact in proper way")]
        public long MindContactNumber { get; set; }

        [DisplayName("Enter Mind Address : ")]
        [Required(ErrorMessage = "Address is required")]
        public string Address { get; set; }

        [DisplayName("Select Mind's Lead: ")]
        public LeadModel Lead { get; set; }
    }
}