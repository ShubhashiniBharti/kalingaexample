﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Entities;

namespace Kalinga_management_system.Models
{
    public class Mapping
    {
        public Lead ConvertLeadModelToEntity(LeadModel lead)
        {
            Lead leadEntity = new Lead();
            leadEntity.LeadId = lead.LeadId;
            leadEntity.LeadName = lead.LeadName;
            leadEntity.LeadContactNumber = lead.LeadContactNumber;
            leadEntity.Category = lead.Category;
            return leadEntity;
        }

        public Mind ConvertMindModelToEntity(MindModel mindModel)
        {
            Mind mind = new Mind();
            mind.MindId = mindModel.MindId;
            mind.MindName = mindModel.MindName;
            mind.DOJ = mindModel.DOJ;
            mind.MindContactNumber = mindModel.MindContactNumber;
            mind.Address = mindModel.Address;

            return mind;
        }

        public List<LeadModel> ConvertLeadFromEntityToModelList(List<Lead> leads)
        {
            List<LeadModel> leadModels = new List<LeadModel>();
            leads.ForEach(data =>
            {
                leadModels.Add(ConvertLeadFromEntityToModel(data));
            });

            return leadModels;
        }

        private LeadModel ConvertLeadFromEntityToModel(Lead data)
        {
            LeadModel leadModel = new LeadModel()
            {
                LeadId = data.LeadId,
                LeadName = data.LeadName,
                LeadContactNumber = data.LeadContactNumber,
                Category = data.Category
            };
            return leadModel;
        }

        public List<MindModel> ConvertMindFromEntityToModelList(List<Mind> minds)
        {
            List<MindModel> mindModels = new List<MindModel>();

            foreach (var item in minds)
            {
                mindModels.Add(ConvertMindFromEntityToModel(item));
            }
            return mindModels;
        }

        private MindModel ConvertMindFromEntityToModel(Mind item)
        {
            MindModel mindModel = new MindModel()
            {
                MindId = item.MindId,
                MindName = item.MindName,
                MindContactNumber = item.MindContactNumber,
                Address = item.Address,
                DOJ = item.DOJ,
                Lead = new LeadModel()
                {
                    LeadId = item.Lead.LeadId,
                    LeadName = item.Lead.LeadName,
                    LeadContactNumber = item.Lead.LeadContactNumber,
                    Category = item.Lead.Category
                }
            };
            return mindModel;
        }
    }
}