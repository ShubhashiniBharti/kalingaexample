﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CustomExceptions
{
    public class NoDataFoundException : Exception
    {
        public NoDataFoundException(string message) : base(message)
        {

        }
    }
}
