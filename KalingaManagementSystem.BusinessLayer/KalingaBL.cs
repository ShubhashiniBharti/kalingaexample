﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using KalingaManagementSystem.DataAccessLayer;

namespace KalingaManagementSystem.BusinessLayer
{
     public class KalingaBL
     {
         private readonly KalingaDAL kalingaDal = new KalingaDAL();
        public int AddLeadDetails(Lead convertLeadModelToEntity)
        {
            return kalingaDal.AddLeadDetails(convertLeadModelToEntity);
        }

        public List<Lead> GetAllLeadsDetailsFromDatabase()
        {
            return kalingaDal.GetAllLeadsDetailsFromDatabase();
        }

        public int AddMindDetail(Mind convertMindModelToEntity, Lead convertLeadModelToEntity)
        {
            return kalingaDal.AddMindDetail(convertMindModelToEntity, convertLeadModelToEntity);

        }

        public List<Lead> GetAllLeadsDetailsFromDatabaseDatabase()
        {
            return kalingaDal.GetAllLeadsDetailsFromDatabase();
        }

        public int UpdateLeadDetail(Lead convertLeadModelToEntity)
        {
            return kalingaDal.UpdateLeadDetail(convertLeadModelToEntity);
        }

        public List<Mind> GetAllMindsDetailsFromDatabase()
        {
            return kalingaDal.GetAllMindsDetailsFromDatabase();
        }

        public int UpdateMindDetail(Lead convertLeadModelToEntity, Mind convertMindModelToEntity)
        {
            return kalingaDal.UpdateMindDetail(convertLeadModelToEntity, convertMindModelToEntity);
        }
     }
}
