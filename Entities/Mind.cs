﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Mind
    {
        public int MindId { get; set; }
        public string MindName { get; set; }
        public DateTime DOJ { get; set; }
        public long MindContactNumber { get; set; }
        public string Address { get; set; }
        public Lead Lead { get; set; }
    }
}
