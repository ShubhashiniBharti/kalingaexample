﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entities
{
    public class Lead
    {
        public int LeadId { get; set; }
        public string LeadName { get; set; }
        public long LeadContactNumber { get; set; }
        public string Category { get; set; }
    }
}
