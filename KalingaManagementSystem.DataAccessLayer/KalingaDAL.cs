﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CustomExceptions;
using Entities;

namespace KalingaManagementSystem.DataAccessLayer
{
    public class KalingaDAL
    {
        private readonly DatabaseConnection databaseConnection = new DatabaseConnection();
        public int AddLeadDetails(Lead convertLeadModelToEntity)
        {
            int affectedRow = 0;
            try
            {
                databaseConnection.OpenConnection();
                SqlCommand cmd = new SqlCommand("spAddLeadDetail", databaseConnection.GetConnection())
                    { CommandType = CommandType.StoredProcedure };

                SqlParameter par;

                par = cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                par.Value = convertLeadModelToEntity.LeadName;

                par = cmd.Parameters.Add("@contact", SqlDbType.BigInt);
                par.Value = convertLeadModelToEntity.LeadContactNumber;

                par = cmd.Parameters.Add("category", SqlDbType.VarChar, 50);
                par.Value = convertLeadModelToEntity.Category;

                affectedRow = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Unable To Access DataBase Please try again", ex);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }

            return affectedRow;
        }

        public List<Lead> GetAllLeadsDetailsFromDatabase()
        {
            List<Lead> leads = new List<Lead>();

            try
            {
                databaseConnection.OpenConnection();
                SqlCommand cmd = new SqlCommand("spGetAllLeadsDetail", databaseConnection.GetConnection()) { CommandType = CommandType.StoredProcedure };
                //SqlCommand cmd = new SqlCommand("Select * from ead", databaseConnection.GetConnection()) { CommandType = CommandType.Text };
                var leadsDetail = cmd.ExecuteReader();

                if (!leadsDetail.HasRows)
                {
                    throw new NoDataFoundException("No data found in database");
                }
                while (leadsDetail.Read())
                {
                    Lead lead = new Lead()
                    {
                        LeadId = leadsDetail.GetInt32(0),
                        LeadName = leadsDetail.GetString(1),
                        LeadContactNumber = leadsDetail.GetInt64(2),
                        Category = leadsDetail.GetString(3)
                    };
                    leads.Add(lead);
                }

                leadsDetail.Close();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Unable to connect with database", ex);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }
            return leads;
        }

        public int AddMindDetail(Mind convertMindModelToEntity, Lead convertLeadModelToEntity)
        {
            int rowAffected;

            try
            {
                databaseConnection.OpenConnection();
                SqlCommand cmd = new SqlCommand("spAddMindDetail", databaseConnection.GetConnection())
                    { CommandType = CommandType.StoredProcedure };


                SqlParameter par;

                par = cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                par.Value = convertMindModelToEntity.MindName;

                par = cmd.Parameters.Add("@doj", SqlDbType.DateTime);
                par.Value = convertMindModelToEntity.DOJ;

                par = cmd.Parameters.Add("@contact", SqlDbType.BigInt);
                par.Value = convertMindModelToEntity.MindContactNumber;

                par = cmd.Parameters.Add("@address", SqlDbType.VarChar, 100);
                par.Value = convertMindModelToEntity.Address;

                par = cmd.Parameters.Add("@lead_id", SqlDbType.Int);
                par.Value = convertLeadModelToEntity.LeadId;

                rowAffected = cmd.ExecuteNonQuery();

            }
            catch (SqlException e)
            {
                throw new CustomSqlException("Not Able to insert data into database", e);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }

            return rowAffected;
        }

        public int UpdateLeadDetail(Lead convertLeadModelToEntity)
        {
            int rowAffected;

            try
            {
                databaseConnection.OpenConnection();

                SqlCommand cmd = new SqlCommand("spUpdateLeadDetail", databaseConnection.GetConnection())
                {
                    CommandType = CommandType.StoredProcedure
                };
                SqlParameter par;

                par = cmd.Parameters.Add("@id", SqlDbType.Int);
                par.Value = convertLeadModelToEntity.LeadId;

                par = cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                par.Value = convertLeadModelToEntity.LeadName;

                par = cmd.Parameters.Add("@contact", SqlDbType.BigInt);
                par.Value = convertLeadModelToEntity.LeadContactNumber;

                par = cmd.Parameters.Add("@category", SqlDbType.VarChar, 50);
                par.Value = convertLeadModelToEntity.Category;

                rowAffected = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Unable to connect to data base !!!", ex);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }
            return rowAffected;
        }

        public List<Mind> GetAllMindsDetailsFromDatabase()
        {
            List<Mind> minds = new List<Mind>();

            try
            {
                databaseConnection.OpenConnection();

                SqlCommand cmd = new SqlCommand("Select * From mind", databaseConnection.GetConnection())
                    { CommandType = CommandType.Text };

                var mindsDetail = cmd.ExecuteReader();

                if (!mindsDetail.HasRows)
                {
                    throw new NoDataFoundException("No Details Found in Database !!!");
                }
                while (mindsDetail.Read())
                {
                    Mind mind = new Mind()
                    {
                        MindId = mindsDetail.GetInt32(0),
                        MindName = mindsDetail.GetString(1),
                        DOJ = mindsDetail.GetDateTime(2),
                        MindContactNumber = mindsDetail.GetInt64(3),
                        Address = mindsDetail.GetString(4),
                        Lead = new Lead()
                        {
                            LeadId = mindsDetail.GetInt32(5)
                        }
                    };

                    minds.Add(mind);
                }
                mindsDetail.Close();
            }
            catch (SqlException ex)
            {
                throw new CustomSqlException("Unable to retrieve data from database", ex);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }

            return minds;
        }

        public int UpdateMindDetail(Lead lead, Mind mind)
        {
            int rowAffected;

            try
            {
                databaseConnection.OpenConnection();

                SqlCommand cmd = new SqlCommand("spUpdateMindDetail", databaseConnection.GetConnection())
                    { CommandType = CommandType.StoredProcedure };


                SqlParameter par;

                par = cmd.Parameters.Add("@id", SqlDbType.Int);
                par.Value = mind.MindId;

                par = cmd.Parameters.Add("@name", SqlDbType.VarChar, 50);
                par.Value = mind.MindName;

                par = cmd.Parameters.Add("@doj", SqlDbType.DateTime);
                par.Value = mind.DOJ;

                par = cmd.Parameters.Add("@contact", SqlDbType.BigInt);
                par.Value = mind.MindContactNumber;

                par = cmd.Parameters.Add("@address", SqlDbType.VarChar, 100);
                par.Value = mind.Address;

                par = cmd.Parameters.Add("@lead_id", SqlDbType.Int);
                par.Value = lead.LeadId;

                rowAffected = cmd.ExecuteNonQuery();
            }
            catch (SqlException ex)
            {

                throw new CustomSqlException("Unable to connect to database", ex);
            }
            finally
            {
                databaseConnection.CloseConnection();
            }
            return rowAffected;
        }
    }
}
