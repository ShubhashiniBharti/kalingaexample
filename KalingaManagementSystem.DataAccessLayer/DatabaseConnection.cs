﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KalingaManagementSystem.DataAccessLayer
{
    class DatabaseConnection
    {
        private static readonly string connectionString = ConfigurationManager.ConnectionStrings["Dbcon"].ConnectionString;
        private readonly SqlConnection sqlConnection = new SqlConnection(connectionString);

        public SqlConnection GetConnection()
        {

            return sqlConnection;
        }

        public void OpenConnection()
        {
            if (sqlConnection.State == System.Data.ConnectionState.Closed)
            {

                sqlConnection.Open();
            }
        }

        public void CloseConnection()
        {
            if (sqlConnection.State == System.Data.ConnectionState.Open)
            {

                sqlConnection.Close();
            }
        }
    }
}
